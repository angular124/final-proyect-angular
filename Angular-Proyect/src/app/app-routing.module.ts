import { AdminModule } from './pages/admin/admin.module';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
{
  path:"home", loadChildren:()=>import("./pages/home/home.module").then(m=>m.HomeModule)
},
{
  path:"admin", loadChildren:()=>import("./pages/admin/admin.module").then(m=>m.AdminModule)
},
{
  path:"nasa", loadChildren:()=>import("./pages/nasa/nasa.module").then(m=>m.NasaModule)
},
{
  path:"spacex", loadChildren:()=>import("./pages/spacex/spacex.module").then(m=>m.SpacexModule)
},
{path:'',redirectTo:"home",pathMatch:"full"},
{path:'**',redirectTo:"home",pathMatch:"full"},

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
