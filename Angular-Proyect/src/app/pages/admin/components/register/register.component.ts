import { ComunityService } from './../../../../service/comunity.service';
import { UserRegisterModel } from './../../models/UserRegistarModel';

import { AbstractControlOptions, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Component, OnInit } from '@angular/core';

import { comparePassword } from './customValidator';
@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {

  public userRegister:FormGroup=new FormGroup({})
  public submitted:boolean=false;


  constructor(
    private formBuilder:FormBuilder,
    private comunityService:ComunityService
  ) {
    this.userRegister=this.formBuilder.group({
    name:["",[Validators.required, Validators.maxLength(20)]],
    nick:["",[Validators.required, Validators.maxLength(10)]],
    profile_img:["",[]],
    email:["",[Validators.required, Validators.email]],
    password:["",[Validators.required,Validators.pattern(/^(?=.*[A-Za-z])(?=.*\d)[A-Za-z\d]{8,}$/)]],
    repeatPassword:["",[Validators.required,Validators.pattern(/^(?=.*[A-Za-z])(?=.*\d)[A-Za-z\d]{8,}$/)]],
    country:["",[Validators.pattern(/^[a-zA-Z]+$/)]],
    city:["",[]],
    birth_date:["",[Validators.pattern(/^(?:(?:31(\/|-|\.)(?:0?[13578]|1[02]))\1|(?:(?:29|30)(\/|-|\.)(?:0?[13-9]|1[0-2])\2))(?:(?:1[6-9]|[2-9]\d)?\d{2})$|^(?:29(\/|-|\.)0?2\3(?:(?:(?:1[6-9]|[2-9]\d)?(?:0[48]|[2468][048]|[13579][26])|(?:(?:16|[2468][048]|[3579][26])00))))$|^(?:0?[1-9]|1\d|2[0-8])(\/|-|\.)(?:(?:0?[1-9])|(?:1[0-2]))\4(?:(?:1[6-9]|[2-9]\d)?\d{2})$/)]],

  },
  {
    validator: comparePassword('password', 'repeatPassword'),
  } as AbstractControlOptions)
  this.userRegister.statusChanges.subscribe(status=>{
    console.log(status);
  })
  //  this.userRegister.valueChanges.subscribe(changes=>{
  //     console.log(changes);
  //  })
}

  ngOnInit(): void {
  }
  onSubmit():any{
    this.submitted=true
    if (this.userRegister.valid) {
    const user:UserRegisterModel={
      name:this.userRegister.get('name')?.value,
      nick:this.userRegister.get('nick')?.value,
      profile_img:this.userRegister.get('profile_img')?.value,
      email:this.userRegister.get('email')?.value,
      password:this.userRegister.get('password')?.value,
      repeatPassword:this.userRegister.get('repeatPassword')?.value,
      country:this.userRegister.get('country')?.value,
      city:this.userRegister.get('city')?.value,
      birth_date:this.userRegister.get('birth_date')?.value,
    }
    console.log(this.userRegister.value);
    console.log(user);
    console.log("Sending form");
    this.comunityService.sendUserComunity(user)
    this.userRegister.reset();
    }else{
      console.log("incorrect Form");
    }


    this.submitted=false
  }
  click(){
    console.log("hola");

  }

}
