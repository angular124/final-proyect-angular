import { FormGroup } from '@angular/forms';


export function comparePassword (passwordName: string, repeatPassword: string) {
  return (formGroup: FormGroup) => {

    const passwordControl = formGroup.controls[passwordName];
    const passwordRepeatControl = formGroup.controls[repeatPassword];

    if (passwordRepeatControl.errors && !passwordRepeatControl.errors['mustMatch']) {
      return;
    }
		
    if (passwordControl.value !== passwordRepeatControl.value) {
      passwordRepeatControl.setErrors({ mustMatch: true });
    } else {
      passwordRepeatControl.setErrors(null);
    }
  };
}
