import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SaveElementsComponent } from './save-elements.component';

describe('SaveElementsComponent', () => {
  let component: SaveElementsComponent;
  let fixture: ComponentFixture<SaveElementsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SaveElementsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SaveElementsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
