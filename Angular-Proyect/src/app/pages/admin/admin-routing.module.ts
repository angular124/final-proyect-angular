import { SaveElementsComponent } from './components/save-elements/save-elements.component';
import { AdminModule } from './admin.module';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AdminComponent } from './admin.component';
import { LoginComponent } from './components/login/login.component';
import { RegisterComponent } from './components/register/register.component';
import { ComunityComponent } from './components/comunity/comunity.component';

const routes: Routes = [
  {
    path:"", component:AdminComponent
  },
  {
    path:"login", component:LoginComponent
  },
  {
    path:"register", component:RegisterComponent
  },
  {
    path:"savedElements", component:SaveElementsComponent
  },
  {
    path:"comunity", component:ComunityComponent
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AdminRoutingModule { }
