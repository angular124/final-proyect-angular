export interface UserRegisterModel{
  name:string
  nick:string,
  profile_img:string
  email:string
  password:string
  repeatPassword:string
  country:string
  city:string,
  birth_date:number
}
