import { StarsService } from './services/stars.service';
import { PhotoOfDay } from './../../../../../../../Angular-miniproyect/miniproyect/src/app/pages/models/Products';
import { NewsService } from './services/news.service';
import { RoversPhotosService } from './services/rovers-photos.service';
import { Component, OnInit } from '@angular/core';
import { RoverModel } from './models/RoversModel';
import { NewsModel } from './models/NewsModel';
import { PhotoOfDayService } from './services/photo-of-day.service';
import { StarModel } from './models/StarsModel';

@Component({
  selector: 'app-nasa',
  templateUrl: './nasa.component.html',
  styleUrls: ['./nasa.component.scss']
})
export class NasaComponent implements OnInit {
  roverList:RoverModel[]=[]
  newsList:NewsModel[]=[]
  starsList:StarModel[]=[]
  loaderActive:boolean=false
  photoDay:PhotoOfDay
  date:any
  photos:any;
filterStars:StarModel[]=[]
  pageRover:number=1;


  constructor(public RoverCall:RoversPhotosService,
    public NewsCall:NewsService,
    public PhotoCall:PhotoOfDayService,
    public StarsCall:StarsService) { }

  ngOnInit(): void { this.loaderActive=true
    this.getRoverPhoto();
    this.getNews();
    this.getStars();
    this.loaderActive=false
// this.getPhotoOfDay();
};

// ROVER´S PHOTOS
private getRoverPhoto(){
  this.loaderActive=true
  this.RoverCall.getRoverPhoto(this.pageRover).subscribe((data:any) => {
  // console.log('API data recived ==>', data);
  this.photos=data.photos;
 this.roverList=this.photos.map((photos: { earth_date: any; id: any; img_src: any;camera:any; rover:any;})=>({
   earth_date:photos.earth_date,
  id:photos.id,
  img_src:photos.img_src,
  camera:this.getCamera(),
rover: this.getRover()}))
});
// console.log('Data in roverList ==>', this.rovergList);
this.loaderActive=false
}
getRover(){
for (const iterator of this.photos) {
return iterator.rover.name
}
  }
  getCamera(){
    for (const iterator of this.photos) {
      return iterator.camera.full_name
      }
  }
  nextRover(){
    if(this.pageRover<40&&this.pageRover>0){
    this.pageRover++;

    this.getRoverPhoto();
    }
  }
  prewRover(){
    if(this.pageRover>1){
    this.pageRover--;

    this.getRoverPhoto();
    }
  }
  // NEWS
  getNews(){
    this.loaderActive=true
    this.NewsCall.getNews().subscribe((data:any) => {
      // console.log('API data recived ==>', data);
      for (const iterator of data) {
        this.newsList.push(iterator)
      }
     // console.log('Data in newList ==>', this.newsList);
  })
  this.loaderActive=false
}
// Photo Of the Day NASA
dateRecived(date:any){

  // console.log("date recived",date);
  var f = date.date;
const dateString=f.getFullYear()+ "-"+(f.getMonth()+1)+ "-" + f.getDate();
  this.date=dateString;
  // console.log("date transform", this.date);
  this.getPhoto(this.date)
}
getPhoto(date:string){
  this.loaderActive=true
  this.PhotoCall.getPhotoOfDay(date).subscribe((data:PhotoOfDay)=>{
    // console.log('API data recived ==>', data);
    this.photoDay=data;
})
this.loaderActive=false};

// GET MORE IMPORTANT STARS
getStars(){
  this.loaderActive=true
  this.StarsCall.getStars().subscribe((data:any)=>{
    // console.log('API data recived ==>', data);
   for (const iterator of data) {
     this.starsList.push(iterator);
   }
})
this.loaderActive=false};
}
