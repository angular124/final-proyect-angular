
import { MatIconModule } from '@angular/material/icon';
import { CardStarsComponent } from './components/card-stars/card-stars.component';
import { MatButtonModule } from '@angular/material/button';
import { MatInputModule } from '@angular/material/input';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NewsService } from './services/news.service';
import { HttpClientModule } from '@angular/common/http';
import { RoversPhotosService } from './services/rovers-photos.service';
import { PhotoOfDayService } from './services/photo-of-day.service';

import { NasaRoutingModule } from './nasa-routing.module';
import { NasaComponent } from './nasa.component';
import { CardsComponent } from './components/cards/cards.component';
import { CardNewsComponent } from './components/card-news/card-news.component';
import { PhotoOfDayComponent } from './components/photo-of-day/photo-of-day.component';
import {MatProgressSpinnerModule} from '@angular/material/progress-spinner';
import {MatTabsModule} from '@angular/material/tabs'
import {MatCardModule} from '@angular/material/card';
import {MatDividerModule} from '@angular/material/divider';
import {MatProgressBarModule} from '@angular/material/progress-bar';
import {MatDatepickerModule} from '@angular/material/datepicker';
import {MatFormFieldModule } from '@angular/material/form-field';
import { MatNativeDateModule } from '@angular/material/core';
import { StarsService } from './services/stars.service';
import { FindStarsPipe } from './pipes/find-stars.pipe';

@NgModule({
  declarations: [
    NasaComponent,
    CardsComponent,
    CardNewsComponent,
    PhotoOfDayComponent,
    CardStarsComponent,
    FindStarsPipe,


  ],
  imports: [
    CommonModule,
    NasaRoutingModule,

    MatTabsModule,
    MatCardModule,
    MatDividerModule,
    MatProgressBarModule,
    MatDatepickerModule,
    MatFormFieldModule,
    ReactiveFormsModule,
    MatNativeDateModule,
    MatInputModule,
    MatButtonModule,
    MatIconModule,
    FormsModule,
    MatProgressSpinnerModule,


    HttpClientModule
  ],
  providers:[RoversPhotosService,
    NewsService,
    PhotoOfDayService,
    StarsService,
  ]
})
export class NasaModule { }
