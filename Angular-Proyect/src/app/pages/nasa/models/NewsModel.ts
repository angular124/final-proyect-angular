export interface NewsModel{
  source:string,
  title:string,
  url:string,
}
