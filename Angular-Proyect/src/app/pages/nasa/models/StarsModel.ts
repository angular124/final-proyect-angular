export interface StarModel{
  distance: number,
  link: string,
  name: string,
  rank: number,
  spectralClass: string
  visualMagnitude: number
}

