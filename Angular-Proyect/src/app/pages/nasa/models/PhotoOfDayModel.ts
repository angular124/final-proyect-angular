export interface PhotoOfDay{
  copyright:string,
  date:string,
  explanation:string,
  title:string,
  url:string,
}
