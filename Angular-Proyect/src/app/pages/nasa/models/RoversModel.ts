export interface RoverModel{
  earth_date:number,
  id:number,
  img_src:string,
  camera:string,
  rover:any,
}
