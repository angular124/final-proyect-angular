import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CardStarsComponent } from './card-stars.component';

describe('CardStarsComponent', () => {
  let component: CardStarsComponent;
  let fixture: ComponentFixture<CardStarsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CardStarsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CardStarsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
