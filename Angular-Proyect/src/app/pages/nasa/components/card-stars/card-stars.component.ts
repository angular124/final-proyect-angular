import { Component, Input, OnInit } from '@angular/core';
import { StarModel } from '../../models/StarsModel';

@Component({
  selector: 'app-card-stars',
  templateUrl: './card-stars.component.html',
  styleUrls: ['./card-stars.component.scss']
})
export class CardStarsComponent implements OnInit {
@Input() stars:StarModel;
  constructor() { }

  ngOnInit(): void {
  }

}
