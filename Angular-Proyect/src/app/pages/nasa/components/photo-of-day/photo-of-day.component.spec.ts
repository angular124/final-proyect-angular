import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PhotoOfDayComponent } from './photo-of-day.component';

describe('PhotoOfDayComponent', () => {
  let component: PhotoOfDayComponent;
  let fixture: ComponentFixture<PhotoOfDayComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PhotoOfDayComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PhotoOfDayComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
