import { PhotoOfDay } from './../../models/PhotoOfDayModel';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Component, EventEmitter, OnInit, Output, Input } from '@angular/core';

@Component({
  selector: 'app-photo-of-day',
  templateUrl: './photo-of-day.component.html',
  styleUrls: ['./photo-of-day.component.scss']
})
export class PhotoOfDayComponent implements OnInit {
  @Output() sendForm=new EventEmitter<any>();
  @Input() photoDay:PhotoOfDay;
  active:boolean=false
  public dateForm:FormGroup = new FormGroup({})
  submitted:boolean=false;
  constructor(private formBuillder:FormBuilder,) {
    this.dateForm=this.formBuillder.group(
      {
        date:["",[Validators.required]],
      }
    )
  }

  ngOnInit(): void {
  }
  sendDate(){
    if (this.dateForm.valid) {
    this.submitted=true;
  this.sendForm.emit(this.dateForm.value);
  // console.log("Se emite =>", this.dateForm.value);
    }
    this.active=true
    // this.dateForm.reset();
    this.submitted=false
  }
}
