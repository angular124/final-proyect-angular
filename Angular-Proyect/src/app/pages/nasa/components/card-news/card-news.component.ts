import { AddNoticeService } from './../../../../service/add-notice.service';
import { Component, Input, OnInit } from '@angular/core';
import { NewsModel } from '../../models/NewsModel';


@Component({
  selector: 'app-card-news',
  templateUrl: './card-news.component.html',
  styleUrls: ['./card-news.component.scss']
})
export class CardNewsComponent implements OnInit {
@Input() news:NewsModel={
  source:"A Paper",
  title:"Something in a Galaxy",
  url:"Fake New"
}


  constructor(public serviceNotice:AddNoticeService) { }

  ngOnInit(): void {
  }
addNotice(){
this.serviceNotice.sendNoticeObservable(this.news)
console.log("Send new",this.news);

}
}

