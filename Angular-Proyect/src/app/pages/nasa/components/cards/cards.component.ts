import { Component, Input, OnInit } from '@angular/core';
import { RoverModel } from '../../models/RoversModel';

@Component({
  selector: 'app-cards',
  templateUrl: './cards.component.html',
  styleUrls: ['./cards.component.scss']
})
export class CardsComponent implements OnInit {
@Input() roverPhoto:RoverModel={
  earth_date:2022,
  id:0,
  img_src:"AltPhoto",
  camera:"FHAZ",
  rover:"Curiosity",
}
  constructor() { }

  ngOnInit(): void {
  }

}
