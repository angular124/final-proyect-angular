import { Input, Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'findStars'
})
export class FindStarsPipe implements PipeTransform {

  transform(value: any, arg:any):any {
    const resultsStars=[];
    for (const stars of value) {
      if(stars.name.toLowerCase().indexOf(arg)>-1){
        resultsStars.push(stars)
      }
    }
   return resultsStars;
}
}
