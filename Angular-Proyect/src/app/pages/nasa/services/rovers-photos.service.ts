
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { RoverModel } from '../models/RoversModel';



@Injectable()


export class RoversPhotosService {

  constructor(private http:HttpClient) { }

getRoverPhoto(page:number):Observable<any>{
  const data= this.http.get<RoverModel>(`https://api.nasa.gov/mars-photos/api/v1/rovers/curiosity/photos?sol=1000&page=${page}&api_key=NSU8OpfZk2cwg1iqOampvMwEC1hOusg7PCw7hac4`)
  // console.log("ApiCall ==>",data);
return data;
}

}
