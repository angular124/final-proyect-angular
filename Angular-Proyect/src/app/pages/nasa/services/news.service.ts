import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { NewsModel } from '../models/NewsModel';

@Injectable()
export class NewsService {

  constructor(private http:HttpClient) { }
  getNews():Observable<any>{
    const data= this.http.get<NewsModel>(`https://space-news.p.rapidapi.com/news/guardian`,
    {headers:{
      "x-rapidapi-host": "space-news.p.rapidapi.com",
    "x-rapidapi-key": "76d841408emsh51752f32f1a62a7p1cc5bfjsn66e757ba95a1"
    }})
    // console.log("ApiCall ==>",data);
  return data;
  }
}
