import { TestBed } from '@angular/core/testing';

import { PhotoOfDayService } from './photo-of-day.service';

describe('PhotoOfDayService', () => {
  let service: PhotoOfDayService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(PhotoOfDayService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
