import { TestBed } from '@angular/core/testing';

import { RoversPhotosService } from './rovers-photos.service';

describe('RoversPhotosService', () => {
  let service: RoversPhotosService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(RoversPhotosService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
