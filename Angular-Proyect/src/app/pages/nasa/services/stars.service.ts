import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { StarModel } from '../models/StarsModel';

@Injectable()

export class StarsService {

  constructor(private http:HttpClient) { }
  getStars():Observable<any>{
    const data= this.http.get<StarModel>("https://brightest-stars.p.rapidapi.com/brightstars",{
      headers: {
        "x-rapidapi-host": "brightest-stars.p.rapidapi.com",
        "x-rapidapi-key": "76d841408emsh51752f32f1a62a7p1cc5bfjsn66e757ba95a1"
        // "x-rapidapi-host": "space-news.p.rapidapi.com",
        // "x-rapidapi-key": "76d841408emsh51752f32f1a62a7p1cc5bfjsn66e757ba95a1"
      }});
    console.log("ApiCall ==>",data);
  return data;
  }
}
