import { PhotoOfDay } from './../../../../../../../../Angular-miniproyect/miniproyect/src/app/pages/models/Products';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

@Injectable()
export class PhotoOfDayService {

  constructor(private http:HttpClient) { }
  getPhotoOfDay(date:string):Observable<any>{
    const data= this.http.get<PhotoOfDay>(`https://api.nasa.gov/planetary/apod?date=${date}&api_key=NSU8OpfZk2cwg1iqOampvMwEC1hOusg7PCw7hac4`)
    console.log("ApiCall ==>",data);
  return data;
  }

}
