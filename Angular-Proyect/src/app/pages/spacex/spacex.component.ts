import { MissionsService } from './services/missions.service';

import { RocketService } from './services/rocket.service';
import { LayoutModule } from './../../core/layout/layout.module';
import { LaunchesService } from './services/launches.service';
import { Component, OnInit } from '@angular/core';
import { LaunchesModel, LinksModel, RocketModel } from './models/LaunchesModel';
import { RocketsModel } from './models/RocketModel';
import { Subscription } from 'rxjs';
import { DomSanitizer } from '@angular/platform-browser'
import { MissionsModel } from './models/MissionsModel';

@Component({
  selector: 'app-spacex',
  templateUrl: './spacex.component.html',
  styleUrls: ['./spacex.component.scss']
})
export class SpacexComponent implements OnInit {
launchesList:LaunchesModel[]=[]
finalList:LaunchesModel[]=[]
rocketList:RocketsModel[]=[]
missionList:MissionsModel[]=[]
videoEmbed:any;
xd:any
xy:any
xz:any

  constructor( private LaunchesCall:LaunchesService,private RocketCall:RocketService,
    private MissionsCall:MissionsService,
    private sanitizer: DomSanitizer) { }

  ngOnInit(): void {
    this.getLaunches()
    this.getRockets()
    this.getMissions()
  }
  // LAUNCHES
getLaunches(){
  this.LaunchesCall.getLaunches().subscribe((data:any)=>{
    // console.log("Data recived", data);
    this.launchesList=data
    this.finalList=this.launchesList.map((launches:{ flight_number:number,mission_name:string, launch_year:number,launch_date_utc:string, rocket:RocketModel,links:LinksModel,
      details:string;})=>({
        flight_number:launches.flight_number,
  mission_name:launches.mission_name,
  launch_year:launches.launch_year,
  launch_date_utc:launches.launch_date_utc,
  rocket:launches.rocket,
  links:{video_link:this.getUrl(this.getVideoIframe(launches.links.video_link)),article_link:launches.links.article_link,mission_patch:launches.links.mission_patch},
  details:launches.details
      }))
    // console.log("Mapping done", this.finalList);
    }
  )}
getUrl(url:string){
  //  console.log(url);
  this.xd=url
  this.xy=this.xd.changingThisBreaksApplicationSecurity
  // console.log(this.xy);
  return this.xy;
}
getVideoIframe(videoUrl: string) {
  let video, results;
  if (videoUrl === null) {
    return '';
  }
  results = videoUrl?.match('[\\?&]v=([^&#]*)');
  video = (results === null) ? videoUrl : results[1];
// console.log("results==>",results);
// console.log("video==>",video);
 this.videoEmbed =this.sanitizer.bypassSecurityTrustResourceUrl('https://www.youtube.com/embed/'+video);
// console.log(this.videoEmbed);
   return this.videoEmbed;
  }

  // ROCKETS
  getRockets(){
    this.RocketCall.getRockets().subscribe((data:any) => {
        // console.log("Data recived", data);
      this.rocketList=data;
      // console.log(this.rocketList);
    })
  }


  // MISSIONS
  getMissions(){
    this.MissionsCall.getMissions().subscribe((data:any)=>{
       console.log("Data recived", data);
       this.missionList=data;
       console.log("List=>",this.missionList);

    })

  }

}



