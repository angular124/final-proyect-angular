export interface RocketsModel{
  name:string,
  active:boolean,
  cost_per_launch:number,
  country:string,
  company:string,
  flickr_images:string[]
  height:any,
  diameter:any,
  first_flight:string,
  engines:RocketEngine,
  wikipedia:string,
}

export interface RocketEngine{
  number:number,
  type:string,
  version:string
}



