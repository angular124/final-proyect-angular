
export interface LaunchesModel{
  flight_number:number,
  mission_name:string,
  launch_year:number,
  launch_date_utc:string,
  rocket:RocketModel,
  links:LinksModel,
  details:string

}

export interface RocketModel{
  rocket_name:string,
  rocket_type:string,
}
export interface LinksModel{
  mission_patch:string,
  article_link:string,
  video_link:string,

}
