import { MissionsModel } from './../../models/MissionsModel';
import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-card-missions',
  templateUrl: './card-missions.component.html',
  styleUrls: ['./card-missions.component.scss']
})
export class CardMissionsComponent implements OnInit {
@Input() mission:MissionsModel={
  mission_name:"olaala",
  manufactures:"Sony",
  satellites:[],
  description:"any",
  wikipedia:"string"
}
  constructor() { }

  ngOnInit(): void {
  }

}
