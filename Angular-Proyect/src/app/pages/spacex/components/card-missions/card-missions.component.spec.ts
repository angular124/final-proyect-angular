import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CardMissionsComponent } from './card-missions.component';

describe('CardMissionsComponent', () => {
  let component: CardMissionsComponent;
  let fixture: ComponentFixture<CardMissionsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CardMissionsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CardMissionsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
