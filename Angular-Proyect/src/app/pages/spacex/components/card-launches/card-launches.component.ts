import { Component, Input, OnInit } from '@angular/core';
import { LaunchesModel } from '../../models/LaunchesModel';

@Component({
  selector: 'app-card-launches',
  templateUrl: './card-launches.component.html',
  styleUrls: ['./card-launches.component.scss']
})
export class CardLaunchesComponent implements OnInit {
@Input() launch:LaunchesModel
isActive:boolean=false
  constructor() { }

  ngOnInit(): void {
  }
moreInformation(){
  this.isActive=!this.isActive
}
}
