import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CardLaunchesComponent } from './card-launches.component';

describe('CardLaunchesComponent', () => {
  let component: CardLaunchesComponent;
  let fixture: ComponentFixture<CardLaunchesComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CardLaunchesComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CardLaunchesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
