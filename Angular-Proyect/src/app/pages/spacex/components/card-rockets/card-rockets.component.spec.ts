import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CardRocketsComponent } from './card-rockets.component';

describe('CardRocketsComponent', () => {
  let component: CardRocketsComponent;
  let fixture: ComponentFixture<CardRocketsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CardRocketsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CardRocketsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
