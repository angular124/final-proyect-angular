import { DomSanitizer } from '@angular/platform-browser';

import { Component, Input, OnInit } from '@angular/core';
import { __importDefault } from 'tslib';
import { RocketsModel } from '../../models/RocketModel';

@Component({
  selector: 'app-card-rockets',
  templateUrl: './card-rockets.component.html',
  styleUrls: ['./card-rockets.component.scss']
})
export class CardRocketsComponent implements OnInit {
  isActive:boolean=false
@Input() rocket:RocketsModel={
    name:"gtrg",
    active:true,
    cost_per_launch:2,
    country:"string",
    company:"string",
    flickr_images:[],
    first_flight:"today",
    height:"[]",
    diameter:"[]",
    engines:{number:4,type:"",version:""},
    wikipedia:"string",
  }
  constructor(private sanitizer:DomSanitizer) { }

  ngOnInit(): void {

  }
getActive(){
  this.isActive=!this.isActive
}

}



