
import { MatDividerModule } from '@angular/material/divider';
import { MatProgressBarModule } from '@angular/material/progress-bar';
import { MissionsService } from './services/missions.service';
import { RocketService } from './services/rocket.service';
import { MatButtonModule } from '@angular/material/button';
import { DomSanitizer } from '@angular/platform-browser';
import { LaunchesService } from './services/launches.service';

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SpacexRoutingModule } from './spacex-routing.module';
import { SpacexComponent } from './spacex.component';

import {MatCardModule} from '@angular/material/card';
import { MatTabsModule } from '@angular/material/tabs';
import { HttpClientModule } from '@angular/common/http';
import { CardLaunchesComponent } from './components/card-launches/card-launches.component';
import { CardMissionsComponent } from './components/card-missions/card-missions.component';
import { CardRocketsComponent } from './components/card-rockets/card-rockets.component';

@NgModule({
  declarations: [
    SpacexComponent,
    CardLaunchesComponent,
    CardMissionsComponent,
    CardRocketsComponent,


  ],
  imports: [
    CommonModule,
    SpacexRoutingModule,
    MatTabsModule,
    MatCardModule,
    MatButtonModule,
    MatProgressBarModule,
    MatDividerModule,


    HttpClientModule,
  ],
  providers:[
    LaunchesService,
    RocketService,
    MissionsService
  ]
})
export class SpacexModule { }
