import { LaunchesModel } from './../models/LaunchesModel';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

@Injectable()
export class LaunchesService {

  constructor(private http:HttpClient) { }
  getLaunches():Observable<any>{
    const data= this.http.get<LaunchesModel>(`https://api.spacexdata.com/v3/launches?limit=50`)
    console.log("ApiCall ==>",data);
  return data;
  }
}
