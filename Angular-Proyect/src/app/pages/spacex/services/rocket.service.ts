import { RocketModel } from './../models/LaunchesModel';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';


@Injectable({
  providedIn: 'root'
})
export class RocketService {

  constructor(private http:HttpClient) { }
  getRockets():Observable<any>{
    const data= this.http.get<RocketModel>(`https://api.spacexdata.com/v4/rockets`)
    console.log("ApiCall ==>",data);
  return data;
  }
}
