import { MissionsModel } from '../models/MissionsModel';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { map, Observable } from 'rxjs';

const url="https://api.spacexdata.com/v3/missions"
@Injectable()
export class MissionsService {

  constructor(private http:HttpClient) { }
  array:MissionsModel[]=[]


   getMissions():Observable<any>{
   return this.http.get<any>(url).pipe(map((response)=>{
    // console.log("Api response =>",response);
  this.array=response.map((response:{mission_name:string,
      manufacturers:any,
      payload_ids:any[],
      description:string,
      wikipedia:string})=>({
        mission_name:response.mission_name,
        manufactures:response.manufacturers,
        satellites: response.payload_ids,
        description:response.description,
        wikipedia:response.wikipedia,
      })
  )
    // console.log("Api response =>",this.array);
  return this.array;
    }))
}
}
