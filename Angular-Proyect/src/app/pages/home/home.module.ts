import { MatIconModule } from '@angular/material/icon';
import { MatButtonModule } from '@angular/material/button';
import { MatDividerModule } from '@angular/material/divider';
import { MatCardModule } from '@angular/material/card';
import { MatProgressBarModule } from '@angular/material/progress-bar';
import { MatMenuModule } from '@angular/material/menu';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { HomeRoutingModule } from './home-routing.module';
import { HomeComponent } from './home.component';
import { WelcomeComponent } from './components/welcome/welcome.component';
import { ContactComponent } from './components/contact/contact.component';
import {MatExpansionModule} from '@angular/material/expansion';
@NgModule({
  declarations: [
    HomeComponent,
    WelcomeComponent,
    ContactComponent
  ],
  imports: [
    CommonModule,
    HomeRoutingModule,
    MatMenuModule,
    MatProgressBarModule,
   MatDividerModule,
    MatCardModule,
    MatButtonModule,
    MatExpansionModule,
    MatIconModule,

  ],
  exports:[
    HomeComponent,
    WelcomeComponent,
    ContactComponent
  ]
})
export class HomeModule { }
