
import { MatIconModule } from '@angular/material/icon';

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { LayoutRoutingModule } from './layout-routing.module';
import { LayoutComponent } from './layout.component';
import { HeaderComponent } from './components/header/header.component';
import { FooterComponent } from './components/footer/footer.component';
import {MatMenuModule} from '@angular/material/menu';

@NgModule({
  declarations: [
    LayoutComponent,
    HeaderComponent,
    FooterComponent,



  ],
  imports: [
    CommonModule,
    LayoutRoutingModule,
    MatIconModule,
    MatMenuModule,
    MatMenuModule,



  ],
  exports:[
    LayoutComponent
  ]
})
export class LayoutModule { }
