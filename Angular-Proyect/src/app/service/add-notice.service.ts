import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';

@Injectable()
export class AddNoticeService {
  private information:BehaviorSubject<any>=new BehaviorSubject(null);
  public list:any[]=[]
  constructor() { }
  getNoticeObservable(): Observable<any>{
    return this.information.asObservable();
  }
  sendNoticeObservable(param:any): void{
    // console.log("Enviando desde servicio",param);
    this.list.push(param)
    this.information.next(param)
  }
}
