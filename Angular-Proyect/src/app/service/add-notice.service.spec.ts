import { TestBed } from '@angular/core/testing';

import { AddNoticeService } from './add-notice.service';

describe('AddNoticeService', () => {
  let service: AddNoticeService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(AddNoticeService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
