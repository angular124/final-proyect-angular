import { UserRegisterModel } from './../pages/admin/models/UserRegistarModel';
import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';

@Injectable( )
export class ComunityService {
  private comunityService:BehaviorSubject<any>=new BehaviorSubject(null)
userList:UserRegisterModel[]=[]
  constructor() { }
  getUserComunity(): Observable<any>{
    return this.comunityService.asObservable();
  }
  sendUserComunity(param:UserRegisterModel): void{
    // console.log("Sending from Service",param);
    this.userList.push(param)
    this.comunityService.next(param)
  }
}
