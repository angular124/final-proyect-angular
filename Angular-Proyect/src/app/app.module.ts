import { AddNoticeService } from './service/add-notice.service';
import { LoaderComponent } from './shared/loader/loader.component';

import { LayoutModule } from './core/layout/layout.module';
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { Button1Component } from './shared/buttons/button1/button1.component';
import { Button2Component } from './shared/buttons/button2/button2.component';
import { MatProgressSpinnerModule} from '@angular/material/progress-spinner';







@NgModule({
  declarations: [
    AppComponent,
    Button1Component,
    Button2Component,
    LoaderComponent,



  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    LayoutModule,
    BrowserAnimationsModule,
    MatProgressSpinnerModule,

  ],
  exports:[LoaderComponent],
  providers: [AddNoticeService],
  bootstrap: [AppComponent]
})
export class AppModule { }
